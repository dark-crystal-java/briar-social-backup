package org.briarproject.briar.android.darkcrystal.simple;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class SimpleFragment extends Fragment {

	protected boolean hasUpNavigation = true;

	@Override
	public void onResume() {
		super.onResume();
		setActionBarNavigation(hasUpNavigation);
	}

	private void setActionBarNavigation(boolean state) {
		AppCompatActivity activity = (AppCompatActivity) getActivity();
		ActionBar actionBar = activity.getSupportActionBar();
		actionBar.setDisplayShowHomeEnabled(state);
		actionBar.setDisplayHomeAsUpEnabled(state);
	}

}
