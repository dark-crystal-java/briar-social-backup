package org.briarproject.briar.android.darkcrystal.simple;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.briarproject.briar.R;
import org.briarproject.briar.android.activity.ActivityComponent;
import org.briarproject.briar.android.darkcrystal.ThresholdDefinedListener;
import org.briarproject.briar.android.fragment.BaseFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static java.util.Objects.requireNonNull;

public class ThresholdSelectorFragment extends SimpleFragment {

	public static final String TAG = ThresholdSelectorFragment.class.getName();

	protected ThresholdDefinedListener listener;

	private SeekBar seekBar;
	private ImageView image;
	private TextView message;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requireNonNull(getActivity()).setTitle(R.string.title_define_threshold);
		setHasOptionsMenu(true);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			@Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_select_threshold,
				container, false);

		seekBar = view.findViewById(R.id.seekBar);
		image = view.findViewById(R.id.imageView);
		message = view.findViewById(R.id.textViewMessage);

		seekBar.setOnSeekBarChangeListener(new SeekBarListener());

		return view;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		listener = (ThresholdDefinedListener) context;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.define_threshold_actions, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_threshold_defined:
				listener.thresholdDefined();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private class SeekBarListener implements SeekBar.OnSeekBarChangeListener {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
            int drawable = progress < 3 ? R.drawable.ic_pie_2_of_5 : R.drawable.ic_pie_2_of_3;
			int text = progress < 3 ? R.string.threshold_insecure : R.string.threshold_secure;
            image.setImageDrawable(getContext().getResources().getDrawable(drawable));
            message.setText(text);
        }

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// do nothing
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// do nothing
		}

	}

}
