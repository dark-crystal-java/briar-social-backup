package org.briarproject.briar.android.darkcrystal.simple;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.briarproject.bramble.api.sync.GroupId;
import org.briarproject.briar.R;
import org.briarproject.briar.android.sharing.ShareForumFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static java.util.Objects.requireNonNull;
import static org.briarproject.briar.android.activity.BriarActivity.GROUP_ID;

public class OwnerRecoveryModeMainFragment extends SimpleFragment {

    public static final String NUM_RECOVERED = "num_recovered";

    public static final String TAG = OwnerRecoveryModeMainFragment.class.getName();

    public static OwnerRecoveryModeMainFragment newInstance(int numRecovered) {
        Bundle args = new Bundle();
        args.putInt(NUM_RECOVERED, numRecovered);
        OwnerRecoveryModeMainFragment fragment = new OwnerRecoveryModeMainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private int numShards;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requireNonNull(getActivity()).setTitle(R.string.title_recovery_mode);

        Bundle args = requireNonNull(getArguments());
        numShards = args.getInt(NUM_RECOVERED);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recovery_owner_main,
                container, false);

        TextView textViewCount = view.findViewById(R.id.textViewShardCount);
        textViewCount.setText(String.format("%d", numShards));

        return view;
    }


}
